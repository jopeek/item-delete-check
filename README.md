# Item Delete Check

### A little module that does just one thing...###
* When you click delete item from the actor inventory a confirmation dialog will appear before deleting the item.

### Installation Instructions

To install a module, follow these instructions:

1. [Download the zip]("https://gitlab.com/tposney/item-delete-check/raw/master/item-delete-check.zip) file included in the module directory. 
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  

Or (preferred)
Paste https://gitlab.com/tposney/item-delete-check/raw/master/module.json into the install module prompt inside Foundry.

